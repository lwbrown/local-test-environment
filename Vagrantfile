# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|
  
  # GitLab Omnibus Box
  config.vm.define "gitlab-omnibus" do |omnibus|
    # VM Image
    omnibus.vm.box = ENV['OMNIBUS_IMAGE'] ? ENV['OMNIBUS_IMAGE'] : "ubuntu/bionic64"

    # VM Private IP
    omnibus.vm.network "private_network", ip: "192.168.33.10"
    
    # VM Configuration
    omnibus.vm.provider "virtualbox" do |vb|
      # Display the VirtualBox GUI when booting the machine
      vb.gui = false
      vb.cpus = 4
      vb.memory = "8192"
    end
  
    omnibus.vm.synced_folder ".", "/vagrant", disabled: true

    if omnibus.vm.box.include? "ubuntu"
      omnibus.vm.provision "shell", inline: <<-SHELL
        sudo apt-get update
        sudo apt-get install -y curl openssh-server ca-certificates
        curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash
        EXTERNAL_URL="http://192.168.33.10" apt install gitlab-ee#{ENV['GL_VERSION'] ? '=' + ENV['GL_VERSION'] + '-ee.0' : ''}
      SHELL
    elsif omnibus.vm.box.include? "centos"
      omnibus.vm.provision "shell", inline: <<-SHELL
        sudo yum install -y curl policycoreutils-python openssh-server
        sudo systemctl enable sshd
        sudo systemctl start sshd
        sudo firewall-cmd --permanent --add-service=http
        sudo firewall-cmd --permanent --add-service=https
        sudo systemctl reload firewalld
        curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash
        EXTERNAL_URL="http://192.168.33.10" yum install -y gitlab-ee#{ENV['GL_VERSION'] ? '-' + ENV['GL_VERSION'] + '-ee.0.el7' : ''}
      SHELL
    end
  end

  # GitLab Runner Box
  config.vm.define "gitlab-runner" do |runner|
    # VM Image
    runner.vm.box = ENV['RUNNER_IMAGE'] ? ENV['RUNNER_IMAGE'] : "ubuntu/bionic64"

    # VM Private IP
    runner.vm.network "private_network", ip: "192.168.33.11"

    # VM Configuration
    runner.vm.provider "virtualbox" do |vb|
      # Display the VirtualBox GUI when booting the machine
      vb.gui = false
      vb.cpus = 2
      vb.memory = "4096"
    end

    runner.vm.synced_folder ".", "/vagrant", disabled: true

    if runner.vm.box.include? "ubuntu"
      runner.vm.provision "shell", inline: <<-SHELL
        curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
        export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E apt-get install gitlab-runner
      SHELL
    elsif runner.vm.box.include? "centos"
      runner.vm.provision "shell", inline: <<-SHELL
        curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh" | sudo bash
        export GITLAB_RUNNER_DISABLE_SKEL=true; sudo -E yum install -y gitlab-runner
      SHELL
    end
  end

  config.vm.define "os-only" do |osonly|
    # VM Image
    osonly.vm.box = ENV['OS_IMAGE'] ? ENV['OS_IMAGE'] : "ubuntu/bionic64"

    # VM Private IP
    osonly.vm.network "private_network", ip: "192.168.33.12"

    # VM Configuration
    osonly.vm.provider "virtualbox" do |vb|
      # Display the VirtualBox GUI when booting the machine
      vb.gui = false
      vb.cpus = 2
      vb.memory = "4096"
    end

    osonly.vm.synced_folder ".", "/vagrant", disabled: true
  end
end
